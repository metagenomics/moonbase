"""
modified version of: https://gitlab.pasteur.fr/metagenomics/snakemake/-/blob/master/tools/metaphlan3/metaphlan/paired/Snakefile
"""

__metaphlan3_exec_command = config.get('metaphlan3', {}).get('exec_command', 'metaphlan')
__metaphlan3_modules = config.get('metaphlan3', {}).get('modules')
__metaphlan3_input_type = config['metaphlan3'].get('input_type', 'fastq')
__metaphlan3_bowtiedb_path = config.get('metaphlan3', {}).get('bowtiedb_path', "")
__metaphlan3_index = config.get('metaphlan3', {}).get('index', "")
__metaphlan3_options = config.get('metaphlan3', {}).get('options', "")
__metaphlan3_threads = config.get('metaphlan3', {}).get('threads', 1)


rule metaphlan3_paired:
    """
    MetaPhlAn 3 can also natively handle paired-end metagenomes (but does not use the paired-end information),
    and, more generally, metagenomes stored in multiple files (but you need to specify the --bowtie2out parameter):

    $ metaphlan metagenome_1.fastq,metagenome_2.fastq --bowtie2out metagenome.bowtie2.bz2 --nproc 5
                    --input_type fastq > profiled_metagenome.txt

    """
    input:
        r1 = __metaphlan3_input_r1,
        r2 = __metaphlan3_input_r2
    output:
        profile = __metaphlan3_output_profile,
        bowtie2out = __metaphlan3_output_bowtie2out,
        sams = __metaphlan3_output_sams
    params:
        exec_command = __metaphlan3_exec_command,
        modules = __metaphlan3_modules,
        input_type = __metaphlan3_input_type,
        bowtiedb_path = __metaphlan3_bowtiedb_path,
        index = __metaphlan3_index,
        options = __metaphlan3_options
    threads:
        __metaphlan3_threads
    run:
        command = []
        if params.modules:
            command.append("module load {params.modules}")
        if params.bowtiedb_path:
            params.options += f" --bowtie2db {params.bowtiedb_path}"
        if params.index:
            params.options += f" --index {params.index}"
        command.append("{params.exec_command} --nproc {threads} --input_type {params.input_type} -s {output.sams} --bowtie2out {output.bowtie2out} {params.options} {input.r1},{input.r2} {output.profile}")
        shell(" && ".join(command))
