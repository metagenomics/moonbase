configfile: "config.yaml"

# ==== Snakefile paths ====
if config['input_data'].get("paired", True):
	__metaphlan3_rules = config.get("snakefiles", {}).get("metaphlan3_paired")
else:
	__metaphlan3_rules = config.get("snakefiles", {}).get("metaphlan3_single")
__metaphlan_merge_rules = config.get("snakefiles", {}).get("metaphlan_merge")

__main_output_dir = config.get('output_dir', 'output')

# ==== Input Data ====
__input_dir = __input_dir = config['input_data']['raw_read_dir']
__input_prefix = config['input_data'].get('pair_prefix', '')
__input_ext = config['input_data'].get('ext', 'fastq.gz')
if config.get('samples', "None") == "None":
	# How samples and paired reads will be detected with wildcards.
	if config["input_data"].get("paired", True):
		(SAMPLES,) = glob_wildcards("{dir}/{{sample}}_{prefix}1.{ext}".format(dir=__input_dir, prefix=__input_prefix, ext=__input_ext))
	else:
		(SAMPLES,) = glob_wildcards("{dir}/{{sample}}.{ext}".format(dir=__input_dir, ext=__input_ext))
else:
	SAMPLES = config['samples']


# ---- Metaphlan3
__metaphlan3_output_dir = __main_output_dir + "/metaphlan3"
__metaphlan3_input_type = config['metaphlan3'].get('input_type', 'fastq')

__metaphlan3_output_profile = "{dir}/{sample}.profile.tsv".format(dir=__metaphlan3_output_dir,
                                                                  sample="{sample}")
__metaphlan3_output_bowtie2out = "{output_dir}/{sample}.bowtie2.bz2".format(output_dir=__metaphlan3_output_dir, sample="{sample}")
__metaphlan3_output_sams = "{output_dir}/{sample}.sam.bz2".format(output_dir=__metaphlan3_output_dir, sample="{sample}")

if config['input_data'].get("paired", True):
	__metaphlan3_input_r1 = "{dir}/{sample}_{prefix}1.{ext}".format(dir=__input_dir,
		                                              sample="{sample}",
		                                              prefix=__input_prefix,
		                                              ext=__input_ext)
	__metaphlan3_input_r2 = "{dir}/{sample}_{prefix}2.{ext}".format(dir=__input_dir,
		                                              sample="{sample}",
		                                              prefix=__input_prefix,
		                                              ext=__input_ext)	
else:
	__metaphlan3_input = "{dir}/{sample}.{ext}".format(dir=__input_dir,
						sample="{sample}",
						ext=__input_ext)
include: __metaphlan3_rules


# ---- Metaphlan merge
__metaphlan_merge_output_dir = __main_output_dir + "/metaphlan_merge"
__metaphlan_merge_output_file_name = config['metaphlan_merge'].get('output_file_name',"merged_taxonomic_profiles.txt")
__metaphlan_merge_input = expand("{dir}/{sample}.profile.tsv".format(dir=__metaphlan3_output_dir,
                                                                      sample="{sample}"),
                                  sample=SAMPLES)
__metaphlan_merge_output = "{dir}/{file_name}".format(dir=__metaphlan_merge_output_dir,
                                                       file_name=__metaphlan_merge_output_file_name)
include: __metaphlan_merge_rules

rule metaphlan_merge_all:
	input:
		"{dir}/{file_name}".format(dir=__metaphlan_merge_output_dir,
                                   file_name=__metaphlan_merge_output_file_name)

# -------- CustomDB Building
__customdb_build_output_dir = __main_output_dir + "/customdb_build"
__custom_db_path = config["output_database"]

# ---- Generating a list of txids to keep
__list_txids_output = "{dir}/{file_name}".format(dir=__customdb_build_output_dir, file_name="list_of_sp_txids_to_keep.txt")

rule list_txids:
	input: __metaphlan_merge_output
	output: __list_txids_output
	run:
		infile = open(str(input), "r")
		lines = infile.read().splitlines()
		infile.close()

		level = 6  # = species -> we filter at the genus level when filtering headers
				
		txids = set()
		i = 0
		# skipping headers
		while lines[i][0] == "#":
				i += 1
		while i < len(lines):
				l = lines[i]
				s = l.split("\t")[1].split("|")
				if len(s) >= level+1:
				    txids.add(s[level])
				i += 1

		with open(str(output), "w") as outfile:
			outfile.write("\n".join(txids)+"\n")

# ---- Filter sequences
__filter_headers_output = "{dir}/{file_name}".format(dir=__customdb_build_output_dir, file_name="headers_to_keep.tmp")
rule filter_headers:
	input: 
		txidsfi=__list_txids_output,
		corrfi=config["prerequisite_files"]["correspondence_file"],
	output: __filter_headers_output
	params:
		level=config['level']	
	shell: "python scripts/filter_headers.py -c {input.corrfi} -t {input.txidsfi} -l {params.level} -o {output}"


__filter_sequences_output = "{dir}/{file_name}".format(dir=__customdb_build_output_dir, file_name="filtered_sequences.fna")
rule filter_sequences:
	input:
		tokeepfi=__filter_headers_output,
		fastaseqfi=config["prerequisite_files"]["multifasta_file"],
	output: __filter_sequences_output
	shell: "python scripts/filter_multifasta.py -hf {input.tokeepfi} -s {input.fastaseqfi} -o {output}"


# ---- Add filtered sequences to library
__add_to_library_output = dynamic(__custom_db_path+"/library/added/prelim_map_{randstring}.txt")
rule add_to_library:
	input:
		fasta=__filter_sequences_output,
	output:
		__add_to_library_output
	params:
		db=__custom_db_path
	threads: 32
	shell: "kraken2-build --add-to-library {input.fasta} --db {params.db} --threads {threads}"


# ---- Build the Custom Kraken DB
__prepare_database_output = "{dir}/{kraken2_taxonomy_dir_name}".format(dir=__custom_db_path, kraken2_taxonomy_dir_name="taxonomy")
rule prepare_database:
	input:
		NCBI_taxonomy_dir=config["kraken2"]["ncbi_tax_dump"],
	output:
		directory(__prepare_database_output)
	params:
		db_path=__custom_db_path
	shell:
		"""
		NCBI_taxonomy_dir={input.NCBI_taxonomy_dir}
		mkdir -p {params.db_path}
		if [ ${{NCBI_taxonomy_dir#*.}} == "tar.gz" ];
		then
			tar -xkf $NCBI_taxonomy_dir -C {params.db_path}
		else
			ln -s $NCBI_taxonomy_dir {output}
		fi
		"""

__build_database_output = "{dir}/{file_name}".format(dir=__custom_db_path, file_name="seqid2taxid.map")
rule build_database:
	input:
		__add_to_library_output,
		__prepare_database_output,
		db=__custom_db_path
	output:
		__build_database_output
	threads: 32
	shell: "kraken2-build --build --db {input.db} --threads {threads}"

# Last rule: Check that the Custom Kraken DB was build correctly and write a report file (mainly to make it the output of rule all)
__check_no_unmapped_and_write_report_output = "{dir}/{file_name}".format(dir=__main_output_dir, file_name="report_file.txt")
rule check_no_unmapped_and_write_report:
	input:
		s2tfi=__build_database_output,
		db=__custom_db_path,
		corrfi=config["prerequisite_files"]["correspondence_file"]
	output:
		__check_no_unmapped_and_write_report_output
	params:
		level=config['level'],
		samples=SAMPLES,
		fastaseqfi=config["prerequisite_files"]["multifasta_file"],
		NCBI_taxonomy_dir=config["kraken2"]["ncbi_tax_dump"],
		m3db_path=config["metaphlan3"].get("bowtiedb_path", ""),
		m3db_index=config["metaphlan3"].get("index", "default")
	threads: 32
	shell:
		"""
		NCBI_taxonomy_dir={params.NCBI_taxonomy_dir}

		# if a `unmapped.txt` file has been created, it means that some accession are missing from the *.accession2taxid files
		if [ -e {input.db}/unmapped.txt ]
		then
		    #grep -f {input.db}/unmapped.txt {input.corrfi} | sed 's/^>\([^\.]*\)\.[^\t]*\t\([0-9]*\)\t.*$/\1\t\2/' >> {input.s2tfi}
		    a2t_newfile=`date +%Y%m%d_%H%M_unmappedacc.accession2taxid`
		    echo -e "accession\taccession.version\ttaxid\tgi" > {input.db}/$a2t_newfile
		    grep -f {input.db}/unmapped.txt {input.corrfi} | sed 's/^>\([^\.]*\)\.\([0-9]\)[^\t]*\t\([0-9]*\)\t.*$/\1\t\1.\2\t\3\t/' >> {input.db}/$a2t_newfile
		    rm {input.db}/unmapped.txt
		    ln -s {input.db}/$a2t_newfile {input.db}/taxonomy/$a2t_newfile
		    kraken2-build --build --db {input.db} --threads {threads}		# and rebuild the kraken2 custom database
		    unlink {input.db}/taxonomy/$a2t_newfile
		fi
		date -r {input.s2tfi} "+Kraken2 customized database: {input.db}; created on %d-%m-%Y (DMY) by running moonbase" > {output}
		LS=$(printf ", %s" {params.samples})
		echo -e "Metadata\n--------\n- samples: ${{LS:2}}\n- level: {params.level}" >> {output}
		echo -e "- input files/directories:\n    * multifasta file: {params.fastaseqfi}\n    * correspondence file: {input.corrfi}" >> {output}
		echo "    * NCBI taxonomy directory: $NCBI_taxonomy_dir" >> {output}
		echo "    * metaphlan database: --bowtie2db {params.m3db_path} --index {params.m3db_index}" >> {output}
		
		if [ -e {input.db}/unmapped.txt ]
		then
			echo "Some accessions couldn't be mapped : See unmapped.txt file."
			echo "You might want to add a '{{filename}}.accession2taxid' file in the taxonomy directory with the unmapped accessions. \
The file must start with a header with the 4 columns name: 'accession', 'accession.version', 'taxid' and 'gi'."
			echo "Then you should rerun kraken2 build: \'kraken2-build --build --db {{custom database path}} --threads {{number of threads}}\`"
		else
			echo -e "Customized Kraken2 database generated.\nNB: The files needed to run kraken2 are the three .k2d files: hash.k2d, opts.k2d, taxo.k2d"
			if [ ${{NCBI_taxonomy_dir#*.}} == "tar.gz" ];
			then
				rm -r {input.db}/taxonomy
			fi
		fi
		"""
		
	

rule all:
	input: __check_no_unmapped_and_write_report_output
