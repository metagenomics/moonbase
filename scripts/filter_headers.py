"""
Headers filtering at genus level
"""

import argparse
import pandas as pd
import sys




##### FUNCTIONS #####


def parse_arguments():
	parser = argparse.ArgumentParser(description='Generating list of headers to keep based on a list of species txids')
	parser.add_argument('-c', '--correspondence_file', required=True, type=str,
		help="correspondence file that associate headers to txids, where 1st column=header; 2nd column=species taxid; 3rd column=genus taxid; 4th columns=sequence source (= kraken). Accept tsv file or tsv.tar.gz")	
	parser.add_argument('-t', '--txids_file', required=True, type=str,
		help="input list of species txids which genus to keep")
	parser.add_argument('-l', '--level', required=True, type=str,
		help="level on which to operate")
	#    ---- optional arguments ----
	parser.add_argument('-o', '--output_file', type=str,
		help="name of the file to stock the headers to keep",
		default="headers_to_keep.txt"
		)
	try:
		return parser.parse_args()
	except SystemExit:
		sys.exit(1)
		

def save_variable_into_file(var, filename):
	outfile = open(filename, "w")
	if type(var) == list or type(var) == set:
		for i in var:
			outfile.write(i+"\n")
	elif type(var) == dict:
		for k, v in var.items():
			if type(v) == tuple:
				strv = str(v[0])+"\t"+str(v[1])
			else:
				strv = str(v)
			outfile.write(str(k)+"\t"+strv+"\n")
	outfile.close()


def filtering_at_species_level(corrdf, txids_to_keep):
	return corrdf[corrdf["metaphlan3_species"].isin(txids_to_keep)]

def filtering_at_genus_level(corrdf, txids_to_keep):
	corrdf["genus"] = corrdf["genus"].fillna(corrdf["metaphlan3_genus"])
	corrdf["genus"] = corrdf["genus"].fillna("genus of "+corrdf["species"])
	
	gtxids_to_keep = set(filtering_at_species_level(corrdf, txids_to_keep)["genus"].unique())
	return corrdf[corrdf["genus"].isin(gtxids_to_keep)]




##### MAIN #####


def main(argv):
	args = parse_arguments()
	txids_to_keep = set(load_list_from_file(args.txids_file))
	
	if args.correspondence_file[-7:] == ".tar.gz":		# looking at the filename's extension
		import tarfile
		with tarfile.open(args.correspondence_file, "r:*") as tar:
			tsv_path = tar.getnames()[0]
			corrdf = pd.read_csv(tar.extractfile(tsv_path), sep="\t", dtype=str)
	else:
		corrdf = pd.read_csv(args.correspondence_file, sep="\t", dtype=str)

	corr_txids_of_interest = eval(f"filtering_at_{args.level}_level(corrdf, txids_to_keep)")

	if args.level == "species":
		args.level = "metaphlan3_species"

	tmp = corr_txids_of_interest.drop_duplicates(subset=args.level, keep="first")
	tmp = tmp[tmp["headers"].isna()][args.level].unique()

	if len(tmp) > 0:
		save_variable_into_file(list(tmp), f"txids_{args.level}_not_found.txt")
		if len(tmp) == 1 or args.level == "species":
			print(f"{len(tmp)} {args.level} not found: saved in txids_{args.level}_not_found.txt")
		else:
			print(f"{len(tmp)} genera not found: saved in txids_genus_not_found.txt")

	save_variable_into_file(list(corr_txids_of_interest["headers"].dropna().drop_duplicates()), args.output_file)
	print(f"{args.output_file} file created")
	
if __name__ == "__main__":
	main(sys.argv[1:])
