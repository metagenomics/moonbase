"""
Filter sequences from a list of headers
"""

import argparse
import sys


##### FUNCTIONS #####


def parse_arguments():
	parser = argparse.ArgumentParser(description='Filtering of sequence multifasta to keep only sequence with headers in a given list of headers to keep')
	parser.add_argument('-s', '--sequence_file', required=True, type=str,
		help="multifasta file")
	parser.add_argument('-hf', '--headers_file', required=True, type=str,
		help="list of headers to keep")
	#    ---- optional arguments ----
	parser.add_argument('-o', '--output_sequence_file', type=str,
		help="name of the filtered multifasta file",
		default="filtered_sequence.fna"
		)
	try:
		return parser.parse_args()
	except SystemExit:
		sys.exit(1)


def filter_and_rewrite_uncompressed_seq_file(sequence_file, tokeep, output_seq_file):
	outfile = open(output_seq_file, "w")
	with open(args.sequence_file) as infile:
		i = 0
		status = False
		for line in infile:
			if line[0] == ">":
				# checking it's the same sequence (header) we're looking at
				if tokeep[i] == line.rstrip():
					outfile.write(line)
					i += 1
					status = True
				else:
					status = False
			else:
				if status:
					outfile.write(line)
				if i == len(tokeep):
					break
	outfile.close()
	return 0

def filter_and_rewrite_compressed_seq_file(sequence_file, tokeep, output_seq_file):
	import tarfile

	outfile = open(output_seq_file, "w")
	
	with tarfile.open(sequence_file, "r:*") as tar:
		fasta_path = tar.getnames()[0]
		with tar.extractfile(fasta_path) as infile:
			i = 0
			status = False
			for line in infile:
				line = line.decode("utf-8")
				if line[0] == ">":
					# checking it's the same sequence (header) we're looking at
					if tokeep[i] == line.rstrip():
						outfile.write(line)
						i += 1
						status = True
					else:
						status = False
				else:
					if status:
						outfile.write(line)
					if i == len(tokeep):
						break
	outfile.close()
	return 0



##### MAIN #####


def main(argv):
	args = parse_arguments()

	infile = open(args.headers_file, "r")
	tokeep = infile.read().splitlines()
	infile.close()

	outfile = open(args.output_sequence_file, "w")

	if args.sequence_file[-7:] == ".tar.gz":		# looking at the filename's extension
		filter_and_rewrite_compressed_seq_file(args.sequence_file, tokeep, args.output_sequence_file)
	else:
		filter_and_rewrite_uncompressed_seq_file(args.sequence_file, tokeep, args.output_sequence_file)
			
	
	if i != len(tokeep):
		print("Warning: All headers to keep aren't found in fasta file.\nYou might want to make sure that headers of both input files are in the same order.")


if __name__ == "__main__":
	main(sys.argv[1:])
