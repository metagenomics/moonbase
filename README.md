# Moonbase

Moonbase is a snakemake [\[1\]](#1-m%C3%B6lder-f-jablonski-kp-letcher-b-hall-mb-tomkins-tinch-ch-sochat-v-forster-j-lee-s-twardziok-so-kanitz-a-et-al-sustainable-data-analysis-with-snakemake-f1000research-2021-10-33-here) workflow that allows you to create a customized Kraken2 [\[2\]](#2-wood-de-lu-j-langmead-b-improved-metagenomic-analysis-with-kraken-2-genome-biol-2019-20-257-here) database using MetaPhlan3's [\[3\]](#3-beghini-f-mciver-lj-blanco-míguez-a-dubois-l-asnicar-f-maharjan-s-mailyan-a-manghi-p-scholz-m-thomas-am-et-al-integrating-taxonomic-functional-and-strain-level-profiling-of-diverse-microbial-communities-with-biobaker) results.

## Citation

_Baud, A.; Kennedy, S.P. Targeted Metagenomic Databases Provide Improved Analysis of Microbiota Samples. Microorganisms 2024, 12, 135._ \[[accessible here](https://doi.org/10.3390/microorganisms12010135)\]

--------

## Install moonbase

▶ <ins>Clone gitlab repository and untar compressed files (depending on free space):</ins>
```bash
git clone https://gitlab.pasteur.fr/metagenomics/moonbase.git
tar -xf metaphlan3bowtie2db.tar.gz
tar -xf metaphlan3bowtie2db/mpa_v30_CHOCOPhlAn_201901.tar
tar -xf prerequisite_files/all_sequences_kraken_and_NCBIassemblies.fna.tar.gz	# optional see recommandations below
tar -xf ncbi_taxonomy_20220914.tar.gz						# optional see recommandations below
```

* all_sequences_kraken_and_NCBIassemblies.fna.tar.gz (45G): to uncompress if you have the space (158G), to allow the workflow to run faster (~5-6 time faster).
* metaphlan3bowtie2db.tar.gz (1,9G): version mpa_v30_CHOCOPhlAn_201901; <ins>to uncompress</ins> (2,9G).
* ncbi_taxonomy_20220914.tar.gz (118M): to uncompress (612M) if you intend to use bracken or if you intend to use the workflow more than once and have the space long term. If given in its compressed format, the directory will be uncompressed during the workflow, but removed at the end.

▶ <ins>Install dependencies:</ins>
* [snakemake](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html)
* [MetaPhlAn3](http://segatalab.cibio.unitn.it/tools/metaphlan/index.html): `conda install -c bioconda metaphlan=3.1.0`
* [Kraken2](https://github.com/DerrickWood/kraken2)
* [python3](https://www.python.org/downloads/) and [pandas](https://pandas.pydata.org/)

--------

## Run workflow

1. Update config.yaml with your **samples**, your **input_data**, your **output_database**, **level** etc. (check [Configfile parameters](#configfile-parameters))
2. Create or load your environment that contains snakemake, kraken2 and metaphlan3 as well as python with the pandas library.
3. Run snakemake: `snakemake -p -j 24 -s Snakefile -R all`
4. <ins>If</ins> an unmapped.txt file is generated when trying to build the customized database, then the missing accessions need to be filled in a {filename}.accession2taxid file in the taxonomy directory of the output database. The file must start with a header with the 4 columns name: 'accession', 'accession.version', 'taxid' and 'gi'.
Finally, re-build the database: `kraken2-build --build --db $DBNAME`

--------

## Output

* in your **output_database** = Kraken2 output:
  * the customized kraken2 database: **hash.k2d**, **opts.k2d**, **taxo.k2d** → You can now run kraken2 with your customized database: `kraken2 --db $DBNAME $SAMPLE_fastq`
  * **seqid2taxid.map**
* in your **output_dir** = workflow intermediate files:
  * in subdirectory **metaphlan3** = metaphlan3 output: for each {sample}, a **{sample}.sam.bz2**, **{samples}.bowtie.bz2** and **{sample}.profile.tsv*
  * in subdirectory **metaphlan_merge**: [**merged_taxonomic_profiles.txt**](#merged_taxonomic_profilestxt). All samples metaphlan3 profiles merged in one file.
  * in subdirectory **customdb_build**:
    * **list_of_sp_txids_to_keep.txt**: list of species found with metaphlan3. So if level chosen is "genus", other species might also be kept ultimately
    * **headers_to_keep.tmp**: headers of multifasta that is kept
    * **filtered_sequences.fna**: all sequences from multifasta used to create the customized kraken2 database

<br><br>


##### **merged_taxonomic_profiles.txt** 
> 1st column = "clade name" = taxonomical lineage as taxonomical names<br>
> 2nd column = "NCBI_tax_id" = taxonomical lineage as taxids<br>
> 3rd to last columns = samples profiles. NB: A column doesn't add up to 100 because the relative abundance is computated at each taxonomical level.<br>

You can use our [moonstone](https://github.com/motleystate/moonstone)'s parser to load it as a relative abundance dataframe in python (where columns add up to 100):
```python
from moonstone.parsers.counts.taxonomy.metaphlan import Metaphlan3Parser
parser = Metaphlan3Parser("merged_taxonomic_profiles.txt")
df = parser.dataframe
```

-------

## Configfile parameters:

* in **snakefiles**,
  * **metaphlan3_paired**: path to Snakefile of the metaphlan3 paired snakemake workflow
  * **metaphlan3_single**: path to Snakefile of the metaphlan3 single snakemake workflow
  * **metaphlan2_merge**: path to Snakefile of the merging of metaphlan results snakemake workflow
* ✏️ **samples**: list of sample names. Do not incorporate extension and pair prefix.
  * _example:_ SAMPLE01_read1.fastq.gz and SAMPLE01_read2.fastq.gz should be listed as SAMPLE01
  * If set to None, all the samples present in the raw read directory that follow the pattern (cf below) will be done
* ✏️ in **input_data**: To be detected the samples must follow the **pattern**: if paired, {sample_name}_{prefix}{read}.{ext}; if single: {sample_name}.{ext}
  * **raw_read_dir**: path to the directory containing the samples
  * **paired**: set to True if samples are paired like in the example above
  * **pair_prefix**: prefix before number in the sample file name
    * _example:_ for SAMPLE01_read1.fastq.gz and SAMPLE01_read2.fastq.gz, pair_prefix should be set as "read"
  * **ext**: extension of samples
    * _example:_ for SAMPLE01_read1.fastq.gz and SAMPLE01_read2.fastq.gz, ext should be set as "fastq.gz"
* **output_dir**: path to the directory that will contain all output of this snakemake workflow.
* ✏️ **output_database**: path to an existing (!) directory where to create the Custom Database
* ✏️ **level**: {species, genus}
  * If level = species, then only the species identified by metaphlan3 are incorporated in the custom kraken2 database
  * If level = genus, then all species belonging to a genus found with metaphlan3, are incorporated in the custom kraken2 database
* in **prerequisite_files**:
  * **multifastafile**: path to the pre-requisite multifasta file, all_sequences_kraken_and_NCBIassemblies.fna(.tar.gz)
  * **correspondencefile**: path to the pre-requisite correspondence file, all_correspondence_headers_kraken_and_metaphlan_taxonomy.tsv(.tar.gz)
* in **metaphlan3**,
  * **threads**: number of threads to use when calling metaphlan
  * **input_type**: {fastq,fasta,bowtie2out,sam} set whether the input is the FASTA file of metagenomic reads or the SAM file of the mapping of the reads against the MetaPhlAn db.
  * **bowtiedb_path**: path to the uncompressed metaphlan3 bowtie database
  * **index**: index name of the metaphlan3 bowtie database
  * **options**: All other possible options declared as you would when calling metaphlan. Check Metaphlan documentation (metaphlan -h)
* in **metaphlan2_merge**,
  * **threads**: number of threads to use for the merging of metaphlan results
* in **kraken2**:
  * **ncbi_tax_dump**: path to the NCBI taxonomy directory

✏️ : to modify absolutely before running snakemake 

-------

## What are all_correspondence_headers_kraken_and_metaphlan_taxonomy.tsv.tar.gz and all_sequences_kraken_and_NCBIassemblies.fna.tar.gz?

##### all_correspondence_headers_kraken_and_metaphlan_taxonomy.tsv.tar.gz 
* compressed format = 7,0M vs uncompressed format = 104M; difference in time between dealing with the compressed and uncompressed file is minor

| header | species | genus | source | metaphlan3_species | metaphlan3_genus |
| ------ | ------ | ------ | ------ | ------ | ------ |
| >kraken:taxid\|1679096\|NZ_CP028858.1 Halococcoides cellulosivorans strain HArcel1T chromosome, complete genome | 1679096 | 2907507 | kraken2 | 1679096 | |
| >kraken:taxid\|1937004\|NZ_CP019470.1 Methanopyrus sp. KOL6 chromosome |  | 2319 | kraken2 | | |
| >NCBI01000028.1 MAG: Chromatiales bacterium 21-64-14 04302015_21_scaffold_100, whole genome shotgun sequence | 1970504 | | GCA_002255365.1* | 1970504 | |
| | 99007 | 2276 | | 99007 | 2276 |

\* NCBI accessionID <br>
<ins>NB:</ins> empty cell for species/metaphlan3_species/metaphlan3_genus = species not in metaphlan3 but from a genus present in metaphlan3 DB<br>
empty cell for genus taxid = No information found for genus lato sensu (txid for genus or no rank/clade below a filled-in family txid)<br>
empty cell for header = species not found (in correspondence file to get its genus if building CustomDB at the `genus` level)


##### all_sequences_kraken_and_NCBIassemblies.fna.tar.gz
* compressed format = 45G vs uncompressed format = 158G; to uncompress if you have the space to allow the workflow to run faster.
* sequences issued from kraken2's bacteria, archaea, fungi, protozoa and viral libraries (657626 sequences), and NCBI assemblies (93992 sequences):
  * 752507 sequences = 562079 _Bacteria_ + 28962 _Archaea_ + 151117 _Eukaryota_ + 10349 _Viruses_
  * 21653 species = 13610 _Bacteria_ + 665 _Archaea_ + 123 _Eukaryota_ + 7255 _Viruses_
  * \>3114 genera = \>2098 _Bacteria_ + \>140 _Archaea_ + 17 _Eukaryota_ + \>859 _Viruses_

-------

## 🌟 Phylogenetic Tree

#####
* RAxML [\[4\]](#4-stamatakis-a-raxml-version-8-a-tool-for-phylogenetic-analysis-and-post-analysis-of-large-phylogenies-bioinformatics-2014-30-13121313-here) phylogenetic tree that contains all 13 610 _Bacteria_ species included in prerequisite multifasta file in **newick** format
* leaves labels follow the format **'{species name}, {txid}'** and **'{species name}, {txid}*'** for species added by add_missing_species rule. 
  * Leaves labels might need to be changed to be used in diversity analyses → One way to do it using [moonstone](https://github.com/motleystate/moonstone):
```python
from moonstone.utils.phylogenetic_tree_editing import adapt_phylogenetic_tree_to_counts_df
parser = SunbeamKraken2Parser(krakenfile)
kraken2_counts = parser.dataframe
adapt_phylogenetic_tree_to_counts_df(kraken2_counts["NCBI_taxonomy_ID"], tree_file, output_tree_file)
```

* Use the phylogenetic tree in diversity analyses, at species level
  * Example: Alpha-diversity analysis
```python
from moonstone.analysis.diversity.alpha import FaithsPhylogeneticDiversity
tree = skbio.TreeNode.read(treefile)
tree_rooted = tree.root_at(tree)
tmp_df = df.groupby("species").sum()
alpha_div_instance = FaithsPhylogeneticDiversity(
	tmp_df[~tmp_df.index.str.contains("(", regex=False)],	# remove rows that aren't defined at the species level (ex: "Lactobacillus (genus)")
	tree_rooted, 
	force_computation=True					# force computation by dropping species that aren't in the tree (viruses, archaea, eukaryota)
)
diversity_indexes = alpha_div_instance.compute_diversity()
```

-------

## References

###### [1] Mölder, F.; Jablonski, K.P.; Letcher, B.; Hall, M.B.; Tomkins-Tinch, C.H.; Sochat, V.; Forster, J.; Lee, S.; Twardziok, S.O.; Kanitz, A.; et al. Sustainable Data Analysis with snakemake. F1000Research 2021, 10, 33. \[[here](https://doi.org/10.12688/f1000research.29032.1)\]
###### [2] Wood, D.E.; Lu, J.; Langmead, B. Improved metagenomic analysis with Kraken 2. Genome Biol. 2019, 20, 257. \[[here](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-019-1891-0)\]
###### [3] Beghini, F.; McIver, L.J.; Blanco-Míguez, A.; Dubois, L.; Asnicar, F.; Maharjan, S.; Mailyan, A.; Manghi, P.; Scholz, M.; Thomas, A.M.; et al. Integrating taxonomic, functional, and strain-level profiling of diverse microbial communities with bioBakery 3. eLife 2021, 10, e65088. \[[here](https://elifesciences.org/articles/65088)\]
###### [4] Stamatakis, A. RAxML version 8: A tool for phylogenetic analysis and post-analysis of large phylogenies. Bioinformatics 2014, 30, 1312–1313. \[[here](https://academic.oup.com/bioinformatics/article/30/9/1312/238053)\]
